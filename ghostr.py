from flask import Flask
import redis

app = Flask(__name__)

#TODO: Add postgres variable here
temp_db = redis.StrictRedis(host='localhost', port=6379, db=0)

@app.route('/')
def main():
    return render_template('index.html')

@app.route('/login')
def signin():
    return render_template('login.html')

@app.route('/register')
def register():
    return render_template('register.html')

@app.route('/user/<name>')
def user(name=None):
    return render_template('user.html')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=3000)
